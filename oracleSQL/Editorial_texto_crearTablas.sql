CREATE TABLE vendedor(
    codigo_vendedor NUMBER GENERATED ALWAYS as IDENTITY(START with 1 INCREMENT by 1) PRIMARY KEY,
    correo VARCHAR2(25) NOT NULL UNIQUE,
    ci varchar2(25) NOT NULL UNIQUE,
    nombre VARCHAR2(25) NOT NULL,
    apellidos VARCHAR2(25) NOT NULL
);

CREATE TABLE contratado(
    codigo_contratado NUMBER GENERATED ALWAYS as IDENTITY(START with 1 INCREMENT by 1) PRIMARY KEY,
    codigo_vendedor NUMBER NOT NULL,
    categoria VARCHAR2(15),
    modelo_coche VARCHAR2(30) NOT NULL,
    CONSTRAINT fk_contratado_vendedor
    FOREIGN KEY (codigo_vendedor) REFERENCES vendedor(codigo_vendedor)
);

CREATE TABLE contrato(
    codigo_contratado NUMBER NOT NULL,
    fecha_alta DATE,
    fecha_baja DATE,
    CONSTRAINT fk_contrato_contratado
    FOREIGN KEY (codigo_contratado) REFERENCES contratado(codigo_contratado)
);

CREATE TABLE promotor(
    codigo_promotor NUMBER GENERATED ALWAYS as IDENTITY(START with 1 INCREMENT by 1) PRIMARY KEY,
    codigo_contratado NUMBER NOT NULL,
    codigo_vendedor NUMBER NOT NULL,
    categoria VARCHAR2(15),
    FOREIGN KEY (codigo_contratado) REFERENCES contratado(codigo_contratado),
    FOREIGN KEY (codigo_vendedor) REFERENCES vendedor(codigo_vendedor)
);

CREATE TABLE delegacion(
    codigo_delegacion NUMBER GENERATED ALWAYS as IDENTITY(START with 1 INCREMENT by 1) PRIMARY KEY,
    nombre_delegacion VARCHAR2(25) NOT NULL,
    correo VARCHAR2(25) NOT NULL UNIQUE
);

CREATE TABLE centro(
    codigo_centro NUMBER GENERATED ALWAYS as IDENTITY(START with 1 INCREMENT by 1) PRIMARY KEY,
    codigo_delegacion NUMBER NOT NULL,
    titular VARCHAR2(25) NOT NULL,
    nombre VARCHAR2(25) NOT NULL,
    CONSTRAINT fk_centro_delegacion FOREIGN KEY (codigo_delegacion) REFERENCES delegacion(codigo_delegacion)
);

CREATE TABLE direccion(
    codigo_centro NUMBER UNIQUE,
    calle VARCHAR2(25),
    numero VARCHAR2(15),
    poblacion NUMBER,
    provincia VARCHAR2(15),
    codigo_postal VARCHAR2(15),
    CONSTRAINT fk_direccion_centro
    FOREIGN KEY (codigo_centro) REFERENCES centro(codigo_centro)
);

CREATE TABLE vendedor_has_centro(
    codigo_vendedor NUMBER NOT NULL,
    codigo_centro NUMBER NOT NULL,
    FOREIGN KEY (codigo_vendedor) REFERENCES vendedor(codigo_vendedor),
    FOREIGN KEY (codigo_centro) REFERENCES centro(codigo_centro)
);

CREATE TABLE visita(
    codigo_visita NUMBER GENERATED ALWAYS as IDENTITY(START with 1 INCREMENT by 1) PRIMARY KEY,
    codigo_centro NUMBER NOT NULL,
    descripcion VARCHAR2(150),
    hora_fin TIMESTAMP,
    FOREIGN KEY (codigo_centro) REFERENCES centro(codigo_centro)
);

CREATE TABLE fecha_visita(
    codigo_visita NUMBER NOT NULL UNIQUE,
    dia DATE,
    hora_ini TIMESTAMP,
    FOREIGN KEY (codigo_visita) REFERENCES visita(codigo_visita)
);

CREATE TABLE vendedor_has_visita(
    codigo_vendedor NUMBER NOT NULL,
    codigo_visita NUMBER NOT NULL,
    FOREIGN KEY (codigo_vendedor) REFERENCES vendedor(codigo_vendedor),
    FOREIGN KEY (codigo_visita) REFERENCES visita(codigo_visita)
);

CREATE TABLE gastos(
    codigo_gastos NUMBER GENERATED ALWAYS as IDENTITY(START with 1 INCREMENT by 1) PRIMARY KEY,
    codigo_visita NUMBER NOT NULL,
    codigo_vendedor NUMBER NOT NULL,
    precio DECIMAL,
    FOREIGN KEY (codigo_visita) REFERENCES visita(codigo_visita),
    FOREIGN KEY (codigo_vendedor) REFERENCES vendedor(codigo_vendedor)
);


CREATE TABLE transporte(
    codigo_gastos NUMBER NOT NULL,
    kms DECIMAL NOT NULL,
    medio VARCHAR2(15) NOT NULL,
    FOREIGN KEY (codigo_gastos) REFERENCES gastos(codigo_gastos)
);

CREATE TABLE alojamiento(
    codigo_gastos NUMBER NOT NULL,
    FOREIGN KEY (codigo_gastos) REFERENCES gastos(codigo_gastos)
);

CREATE TABLE manutencion(
    codigo_gastos NUMBER NOT NULL,
    tipo VARCHAR2(20) NOT NULL,
    FOREIGN KEY (codigo_gastos) REFERENCES gastos(codigo_gastos)
);

















