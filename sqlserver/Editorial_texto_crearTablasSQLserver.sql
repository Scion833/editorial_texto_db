CREATE TABLE Vendedor (
  codigo_vendedor INT PRIMARY KEY IDENTITY(1,1) NOT NULL,
  correo VARCHAR(25) UNIQUE NOT NULL,
  ci VARCHAR(25) UNIQUE NOT NULL,
  nombre VARCHAR(25) NOT NULL,
  apellidos VARCHAR(25) NOT NULL
);


CREATE TABLE Contratado (
  codigo_contratado INT PRIMARY KEY IDENTITY(1,1) NOT NULL,
  codigo_vendedor INT NOT NULL,
  categoria VARCHAR(15) NOT NULL,
  modelo_coche VARCHAR(30) NOT NULL,
  FOREIGN KEY (codigo_vendedor) REFERENCES Vendedor(codigo_vendedor)
);

CREATE TABLE Contrato (
  codigo_contratado INT NOT NULL,
  fecha_alta DATE,
  fecha_baja DATE,
  FOREIGN KEY (codigo_contratado) REFERENCES Contratado(codigo_contratado)
);

CREATE TABLE Promotor (
  codigo_promotor INT PRIMARY KEY IDENTITY(1,1) NOT NULL,
  codigo_contratado INT NOT NULL,
  categoria VARCHAR(15),
  FOREIGN KEY (codigo_contratado) REFERENCES Contratado(codigo_contratado)
);


CREATE TABLE Delegacion (
  codigo_delegacion INT PRIMARY KEY IDENTITY(1,1) NOT NULL,		
  nombre_delegacion VARCHAR(25) NOT NULL,
  correo VARCHAR(25) UNIQUE NOT NULL,
);

CREATE TABLE Centro (
  codigo_centro INT PRIMARY KEY IDENTITY(1,1) NOT NULL,
  codigo_delegacion INT NOT NULL,
  titular VARCHAR(25) NOT NULL,
  nombre VARCHAR(25) NOT NULL,
  FOREIGN KEY (codigo_delegacion) REFERENCES Delegacion(codigo_delegacion)
);

CREATE TABLE Direccion (
	codigo_centro INT UNIQUE NOT NULL,
  calle VARCHAR(25),
  numero VARCHAR(15),
  poblacion INT,
  provincia VARCHAR(15),
  codigo_postal VARCHAR(15),
  FOREIGN KEY (codigo_centro) REFERENCES Centro(codigo_centro)
);

CREATE TABLE Vendedor_has_centro (
  codigo_vendedor INT NOT NULL,
  codigo_centro INT NOT NULL,
  FOREIGN KEY (codigo_vendedor) REFERENCES Vendedor(codigo_vendedor),
  FOREIGN KEY (codigo_centro) REFERENCES Centro(codigo_centro)
);

CREATE TABLE Visita (
  codigo_visita INT PRIMARY KEY IDENTITY(1,1) NOT NULL,
  codigo_centro INT NOT NULL,
  descripcion VARCHAR(150),
  hora_fin DATETIME,
  FOREIGN KEY (codigo_centro) REFERENCES Centro(codigo_centro)
);

CREATE TABLE Fecha_Visita (
  codigo_visita INT NOT NULL,
  dia DATE,
  hora_ini TIME,
  FOREIGN KEY (codigo_visita) REFERENCES Visita(codigo_visita)
);

CREATE TABLE Vendedor_has_visita (
  codigo_vendedor INT NOT NULL,
  codigo_visita INT NOT NULL,
  FOREIGN KEY (codigo_vendedor) REFERENCES Vendedor(codigo_vendedor),
  FOREIGN KEY (codigo_visita) REFERENCES Visita(codigo_visita)
);

CREATE TABLE Gastos (
  codigo_gastos INT PRIMARY KEY IDENTITY(1,1) NOT NULL,
  codigo_visita INT NOT NULL,
  codigo_vendedor INT NOT NULL,
  precio FLOAT NOT NULL,
  FOREIGN KEY (codigo_visita) REFERENCES Visita(codigo_visita),
  FOREIGN KEY (codigo_vendedor) REFERENCES Vendedor(codigo_vendedor)
);

CREATE TABLE Transporte (
  codigo_gastos INT NOT NULL,
  kms FLOAT NOT NULL,
  medio VARCHAR(15) NOT NULL,
  FOREIGN KEY (codigo_gastos) REFERENCES Gastos(codigo_gastos)
);

CREATE TABLE Alojamiento (
  codigo_gastos INT NOT NULL,
  FOREIGN KEY (codigo_gastos) REFERENCES Gastos(codigo_gastos)
);

CREATE TABLE Manutencion (
  codigo_gastos INT NOT NULL,
  tipo VARCHAR(20),
  FOREIGN KEY (codigo_gastos) REFERENCES Gastos(codigo_gastos)
);






